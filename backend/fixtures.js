const mongoose = require('mongoose');
const config = require('./config');

const Question = require('./models/Question');
const User = require('./models/User');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
    try {
        await db.dropCollection('users');
        await db.dropCollection('questions');
    } catch (e) {
        console.log('Collections were not present, skipping drop...');
    }

    const [Mike, Carl] = await User.create({
        username: 'Mike',
        password: '123'
    }, {
        username: 'Carl',
        password: '123'
    });

    await Question.create({
        title: 'Как вы относитесь к животным?',
        image: 'animals.jpg',
        answers: [{text: 'Я обожаю животных'}, {text: 'Безразлично'}, {text: 'Нормально'}],
        userID: Mike._id
    }, {
        title: 'Какое блюдо вы больше всего предпочитаете?',
        image: 'foods.jpg',
        answers: [{text: 'Манты'}, {text: 'Лагман'}, {text: 'Плов'}, {text: 'Шашлык'}],
        userID: Carl._id
    });

    db.close();
});