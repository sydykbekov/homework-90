import React from 'react';
import {Route, Switch} from "react-router-dom";

import NewPoll from "./containers/NewPoll/NewPoll";
import Register from "./containers/Register/Register";
import Products from "./containers/Polls/Polls";
import Login from "./containers/Login/Login";
import Poll from './containers/Poll/Poll';
import MyPolls from "./containers/MyPolls/MyPolls";

const Routes = () => (
    <Switch>
        <Route path="/" exact component={Products}/>
        <Route path="/polls/new" exact component={NewPoll}/>
        <Route path="/register" exact component={Register}/>
        <Route path="/login" exact component={Login}/>
        <Route path="/polls/:id" exact component={Poll}/>
        <Route path="/my-polls" exact component={MyPolls}/>
    </Switch>
);

export default Routes;