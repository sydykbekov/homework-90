import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {NotificationManager} from 'react-notifications';
import {Thumbnail, Button, Badge, FormGroup, Radio, Form, Panel, Col} from 'react-bootstrap';
import FormElement from '../../components/UI/Form/FormElement';
import './Poll.css';
import {addComment, fetchPoll, getComments, vote} from "../../store/actions/polls";

class Poll extends Component {
    state = {
        radioGroup: '',
        comment: ''
    };

    componentDidMount() {
        this.props.getPoll(this.props.match.params.id);
        this.props.getComments(this.props.match.params.id);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    submitFormHandler = event => {
        event.preventDefault();

        if (this.props.user) {
            const answerID = this.state.radioGroup;
            this.props.vote(answerID, this.props.match.params.id, this.props.token, this.props.user._id);
        } else {
            this.props.history.push('/login');
            NotificationManager.error('Error', 'At first, you should be logged in!');
        }
    };

    submitCommentFormHandler = event => {
        event.preventDefault();

        const comment = {
            text: this.state.comment,
            user: this.props.user._id,
            questionId: this.props.match.params.id
        };

        this.props.addComment(comment, this.props.token);
    };

    render() {
        return (
            <Fragment>
                {this.props.poll && <Thumbnail src={'http://localhost:8000/uploads/' + this.props.poll.image} style={{boxShadow: '2px 2px 10px grey'}}>
                    <h2>{this.props.poll.title}</h2>
                    <Form onSubmit={this.submitFormHandler} style={{overflow: 'hidden'}}>
                        <FormGroup style={{display: 'flex', justifyContent: 'center', flexWrap: 'wrap'}}>
                            {this.props.poll.answers.map(variant =>
                                <Radio name="radioGroup" value={variant._id} key={variant._id} inline required onClick={this.inputChangeHandler}>
                                    {variant.text} <Badge style={{backgroundColor: '#265a88'}} pullRight>Votes: {variant.vote}</Badge>
                                </Radio>
                            )}
                        </FormGroup>
                        <Button bsStyle="primary" type="submit" style={{float: 'right', width: '100px'}}>Vote</Button>
                    </Form>
                </Thumbnail>}
                {this.props.user && <Panel>
                    <Panel.Body>
                        <Form horizontal onSubmit={this.submitCommentFormHandler}>
                            <FormElement
                                propertyName="comment"
                                title="Comment"
                                placeholder="comment ..."
                                autoComplete="new-comment"
                                type="text"
                                value={this.state.comment}
                                changeHandler={this.inputChangeHandler}
                                required
                            />
                            <FormGroup>
                                <Col smOffset={2} sm={10}>
                                    <Button
                                        bsStyle="primary"
                                        type="submit"
                                    >Create comment</Button>
                                </Col>
                            </FormGroup>
                        </Form>
                    </Panel.Body>
                </Panel>}
                {this.props.comments.map(comment => <Panel bsStyle="info" style={{width: '90%', margin: '10px auto'}} key={comment._id}>
                    <Panel.Heading>
                        <h4>{comment.user.username}:</h4>
                        {comment.text}
                    </Panel.Heading>
                </Panel>)}
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    poll: state.polls.poll,
    user: state.users.user,
    comments: state.polls.comments,
    token: state.users.token
});

const mapDispatchToProps = dispatch => ({
    getPoll: id => dispatch(fetchPoll(id)),
    vote: (answerID, pollID, token, userID) => dispatch(vote(answerID, pollID, token, userID)),
    addComment: (comment, token) => dispatch(addComment(comment, token)),
    getComments: id => dispatch(getComments(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Poll);
