import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, PageHeader, Grid, Row} from "react-bootstrap";
import {Link} from "react-router-dom";

import PollsListItem from '../../components/PollsListItem/PollsListItem';
import {fetchPolls} from "../../store/actions/polls";

class Polls extends Component {
    componentDidMount() {
        this.props.getPolls();
    }

    render() {
        return (
            <Fragment>
                <PageHeader>
                    Polls
                    {this.props.user &&
                    <Link to="/polls/new">
                        <Button bsStyle="primary" className="pull-right">
                            Add poll
                        </Button>
                    </Link>
                    }
                </PageHeader>
                <Grid>
                    <Row className="show-grid">
                        {this.props.polls.map(poll => (
                            <PollsListItem
                                key={poll._id}
                                id={poll._id}
                                title={poll.title}
                                image={poll.image}
                            />
                        ))}
                    </Row>
                </Grid>
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        user: state.users.user,
        polls: state.polls.polls
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getPolls: () => dispatch(fetchPolls())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Polls);