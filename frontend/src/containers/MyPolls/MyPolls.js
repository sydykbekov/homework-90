import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Grid, PageHeader, Row} from "react-bootstrap";
import PollsListItem from "../../components/PollsListItem/PollsListItem";
import {getMyPolls, removePoll} from "../../store/actions/polls";

class MyPolls extends Component {
    componentDidMount() {
        this.props.getMyPolls(this.props.user._id);
    }

    render() {
        return (
            <Fragment>
                <PageHeader>
                    My Polls
                </PageHeader>
                <Grid>
                    <Row className="show-grid">
                        {this.props.myPolls.map(poll => (
                            <PollsListItem
                                key={poll._id}
                                id={poll._id}
                                title={poll.title}
                                image={poll.image}
                                remove={() => this.props.removePoll(poll._id, this.props.token)}
                            />
                        ))}
                    </Row>
                </Grid>
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
    myPolls: state.polls.myPolls,
    token: state.users.token
});

const mapDispatchToProps = dispatch => ({
    getMyPolls: id => dispatch(getMyPolls(id)),
    removePoll: (pollID, token) => dispatch(removePoll(pollID, token))
});

export default connect(mapStateToProps, mapDispatchToProps)(MyPolls);