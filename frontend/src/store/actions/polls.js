import axios from '../../axios-api';
import {
    FETCH_POLLS_SUCCESS, FETCH_POLL_SUCCESS, FETCH_COMMENTS_SUCCESS, FETCH_MY_POLLS_SUCCESS
} from "./actionTypes";
import {push} from "react-router-redux";
import {NotificationManager} from "react-notifications";

export const fetchPollsSuccess = polls => {
    return {type: FETCH_POLLS_SUCCESS, polls};
};

export const fetchPollSuccess = poll => {
    return {type: FETCH_POLL_SUCCESS, poll};
};

export const fetchCommentsSuccess = comments => {
    return {type: FETCH_COMMENTS_SUCCESS, comments};
};

export const getMyPollsSuccess = polls => {
    return {type: FETCH_MY_POLLS_SUCCESS, polls};
};

export const fetchPolls = () => {
    return dispatch => {
        axios.get('questions').then(response => {
            dispatch(fetchPollsSuccess(response.data));
        })
    }
};

export const createPoll = (formData, token) => {
    return dispatch => {
        const headers = {"Token": token};
        axios.post('questions', formData, {headers}).then(() => {
            NotificationManager.success('success', 'Your poll is created!');
            dispatch(push('/'));
        })
    }
};

export const fetchPoll = pollID => {
    return dispatch => {
        axios.get(`questions/${pollID}`).then(response => {
            dispatch(fetchPollSuccess(response.data));
        })
    }
};

export const vote = (answerID, pollID, token, userID) => {
    return dispatch => {
        const headers = {"Token": token};
        axios.put(`questions/${pollID}`, {answer: answerID, user: userID}, {headers}).then(response => {
            if (response.data.error) {
                NotificationManager.error('error', response.data.error);
            } else {
                dispatch(fetchPoll(pollID));
                NotificationManager.success('success', 'Your vote counted!');
            }
        });
    };
};

export const getComments = pollID => {
    return dispatch => {
        axios.get(`comments/${pollID}`).then(response => {
            dispatch(fetchCommentsSuccess(response.data));
        })
    }
};

export const addComment = (comment, token) => {
    return dispatch => {
        const headers = {"Token": token};
        axios.post('comments', comment, {headers}).then(response => {
            dispatch(getComments(comment.questionId));
            console.log(response);
        })
    };
};

export const getMyPolls = id => {
    return dispatch => {
        axios.get(`questions?userID=${id}`).then(response => {
            dispatch(getMyPollsSuccess(response.data));
        })
    }
};

export const removePoll = (id, token) => {
    return dispatch => {
        const headers = {"Token": token};
        axios.delete(`questions/${id}`, {headers}).then(() => {
            dispatch(push('/'));
            NotificationManager.success('success', 'Your poll was removed!');
        })
    }
};