import React, {Component} from 'react';

import Layout from "./containers/Layout/Layout";
import Routes from "./Routes";
import {withRouter} from "react-router-dom";

class App extends Component {
  render() {
    return (
      <Layout>
        <Routes />
      </Layout>
    );
  }
}

export default withRouter(App);
